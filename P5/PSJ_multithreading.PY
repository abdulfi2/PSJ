import platform as p
import subprocess as sub
import time
import concurrent.futures
import threading

print("="*22,"Start","="*21)
T1 = time.perf_counter()
host = ['192.168.1.1','192.168.1.2','192.168.1.3','8.8.8.8','8.8.4.4']

def do_ping(host):
    systemOs = '-n' if p.system().lower() == 'windows' else '-c'
    pinging = ['ping', systemOs, '1', host]
    status = ''
    time.sleep(1)

    if sub.call(pinging) == 0:
        status = 'UP'
    else:
        status = 'DOWN'

    output = '\nHost {} is {}'.format(host, status)
    print(output)

with concurrent.futures.ThreadPoolExecutor() as executor:
    for i in host:
        result = [executor.submit(do_ping,i)]

T2 = time.perf_counter()

print("="*50)
print('Selesai dalam {} detik'.format(round(T2-T1,2)))
