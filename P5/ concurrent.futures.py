import concurrent.futures
import time
import os

T1 = time.perf_counter()

def do_something(sec):
    print(f"Diam sejenak….. {sec} detik")
    time.sleep(sec)
    return "Selesai berdiam….."

with concurrent.futures.ThreadPoolExecutor() as executor:
    results = [executor.submit(do_something,1) for _ in range(10)]
    for f in concurrent.futures.as_completed(results):
    print(f.result())

T2 = time.perf_counter()
print(f"Selesai dalam … {round(T2-T1,2)} detik")
