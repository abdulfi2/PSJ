import requests
from bs4 import BeautifulSoup

url = 'https://www.bmkg.go.id/gempabumi/gempabumi-terkini.bmkg'
web = requests.get(url)
data = web.text
bs = BeautifulSoup(data,'html.parser')

body = bs.find('tbody')
rows = body.findAll('tr')

for td in rows:
    print('===================================================')
    cols = td.findAll('td')
    print('no\t\t:', (cols[0].text))
    print('waktu\t\t:', (cols[1].text))
    print('lintang\t\t:', (cols[2].text))
    print('bujur\t\t:', (cols[3].text))
    print('magnitudo\t:', (cols[4].text))
    print('kedalaman\t:', (cols[5].text))
    print('wilayah\t\t:', (cols[6].text))
    print('\n')

# thlist = []
# tdlist = []

# head = bs.find('thead')
# rowshead = head.findAll('tr')
# body = bs.find('tbody')
# rows = body.findAll('tr')

# for td in rows:
#     cols = td.findAll('td')
#     for txt in cols:
#         tdlist.append(txt.text)
    
#     for k in tdlist:
#         print(k)

# for row in rowshead:
#     cols = row.findAll('th')
#     for ele in cols:
#         tdlist.append(ele.text)
#         if tdlist[0] == '#':
#             tdlist[0] = 'no'

#     for k in tdlist:
#         print(k,' : ')
